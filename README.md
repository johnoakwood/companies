# Companies

Test project made by Jānis Ozoliņš for Creative.gs

## Getting Started

Project based on a Laravel implementing admin panel and CRUD functionality for list containing companies.

Project features:

* Model for company
* Admin dashboard and company controllers
* Front controller
* Laravel auth
* Password protected admin panel
* Request for company data (validation)
* Bootstrap for views
* View structure
* DB migrations
    * DB seeds (for Users and Company)
    * Factories (for User and Company)
* Custom routing
* Feature tests for Auth and Company
    * Faker for test data generation
* Laravel MIX for style and scripts
* Laravel pagination
* Laravel localization for views & controllers
* Mailtrap for password reset
* Other things I have probably forgotten

### Prerequisites

.env is not included in repo.

To set up DB and seeds:
```
php artisan migrate --seed
```
To compile style/scripts:
```
npm run dev
```
To run project in http://localhost:8000:
```
php artisan serve
```
To run php tests:
```
phpunit
```