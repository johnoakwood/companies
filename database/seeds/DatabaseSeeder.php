<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Company;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seed admin
        $user = factory(User::class)->make([
            'email'    => 'admin@admin.lv',
            'password' => bcrypt('parole'),
        ]);

        $user->save();

        // Seed companies
        for ($i = 0; $i < 100; $i++) {
            $company = factory(Company::class)->make();
            $company->save();
        }
    }
}
