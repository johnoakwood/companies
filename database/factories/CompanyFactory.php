<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Company Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name'     => $faker->name,
        'address'  => $faker->address,
        'homepage' => $faker->domainName,
        'email'    => $faker->companyEmail,
    ];
});
