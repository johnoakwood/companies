<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General use Language Lines
    |--------------------------------------------------------------------------
    */

    'companies' => 'Companies',
    'dashboard' => 'Dashboard',
    'create_new_company' => 'Create new company',
    'companies_list' => 'Companies list',
    'name' => 'Name',
    'address' => 'Address',
    'website' => 'Website',
    'email' => 'Email',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'add_new_company' => 'Add new company',
    'company_name' => 'Company name',
    'company_address' => 'Company address',
    'company_homepage' => 'Company homepage',
    'company_email' => 'Company e-mail',
    'add' => 'Add',
    'admin_dashboard' => 'Admin dashboard',
    'you_are_logged_in' => 'You are logged in, good job!',
    'you_can_go_to_companies' => 'You can go <a href=":route">here</a> to see all companies!',
    'please_login_to_access_admin' => 'Please <a href=":route">login</a> to access administration!',
    'edit_company' => 'Edit company',
    'update' => 'Update',
    'companies_webpage' => 'Companies webpage',
    'company_added' => 'New company has been added',
    'company_updated' => 'Company data has been updated',
    'company_deleted' => 'Company has been deleted',
];