@extends('layouts.app')

@section('title', __('messages.companies'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('messages.companies_webpage') }}</div>
                    <div class="card-body">
                        @auth
                            {!! __('messages.you_can_go_to_companies', ['route' => route('companies-list')]) !!}
                        @else
                            {!! __('messages.please_login_to_access_admin', ['route' => route('login')]) !!}
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection