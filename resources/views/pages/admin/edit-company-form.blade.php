@extends('layouts.app')

@section('title', __('messages.edit_company'))

@section('content')
    <div class="card">
        <div class="card-header">
            {{ __('messages.edit_company') }}
        </div>
        <div class="card-body">

            @include('partials.errors')

            <form method="post" action="{{ route('update-company', $company->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">{{ __('messages.company_name') }}:</label>
                    <input type="text" class="form-control" name="name" value="{{ $company->name }}"/>
                </div>
                <div class="form-group">
                    <label for="address">{{ __('messages.company_address') }}:</label>
                    <input type="text" class="form-control" name="address" value="{{ $company->address }}"/>
                </div>
                <div class="form-group">
                    <label for="homepage">{{ __('messages.company_homepage') }}:</label>
                    <input type="text" class="form-control" name="homepage" value="{{ $company->homepage }}"/>
                </div>
                <div class="form-group">
                    <label for="email">{{ __('messages.company_email') }}:</label>
                    <input type="text" class="form-control" name="email" value="{{ $company->email }}"/>
                </div>
                <button type="submit" class="btn btn-primary">{{ __('messages.update') }}</button>
            </form>
        </div>
    </div>
@endsection