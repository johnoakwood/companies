@extends('layouts.app')

@section('title', __('messages.companies'))

@section('content')
    @if(session()->get('success'))
        <div class="alert alert-success mt-3">
            {{ session()->get('success') }}
        </div><br />
    @endif
    <div class="card">
        <div class="card-header">
            {{ __('messages.companies') }}
        </div>
        <div class="card-body">
            <a class="btn btn-success mb-3" href="{{ route('create-company-form')}}" role="button">{{ __('messages.create_new_company') }}</a>
            <div class="card">
                <div class="card-header">
                    {{ __('messages.companies_list') }}
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">{{ __('messages.name') }}</th>
                            <th scope="col">{{ __('messages.address') }}</th>
                            <th scope="col">{{ __('messages.website') }}</th>
                            <th scope="col">{{ __('messages.email') }}</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <td>{{ $company->name }}</td>
                                    <td>{{ $company->address }}</td>
                                    <td><a href="#">{{ $company->homepage }}</a></td>
                                    <td><a href="mailto:{{ $company->email }}">{{ $company->email }}</a></td>
                                    <td>
                                        <a href="{{ route('edit-company-form', $company->id)}}" class="btn btn-primary">{{ __('messages.edit') }}</a>
                                        <form action="{{ route('delete-company', $company->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">{{ __('messages.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row justify-content-center pt-3">
                        {{ $companies->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection