@extends('layouts.app')

@section('title', __('messages.companies'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('messages.admin_dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p>{{ __('messages.you_are_logged_in') }}</p>
                        <p>{!! __('messages.you_can_go_to_companies', ['route' => route('companies-list')]) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection