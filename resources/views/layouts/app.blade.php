<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Metadata -->
        @include('globals.metadata')

        <!-- Styles -->
        @include('globals.style')
    </head>
    <body>
        {{--@include('partials.login')--}}

        <div id="app">
            <!-- Navigation -->
            @include('globals.navigation')

            <div class="container">
                @yield('content')
            </div>
        </div>

        <!-- Scripts -->
        @include('globals.scripts')
    </body>
</html>