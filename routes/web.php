<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Authorization Routes
|--------------------------------------------------------------------------
*/

Auth::routes(['register' => false]);

/*
|--------------------------------------------------------------------------
| Front Routes
|--------------------------------------------------------------------------
*/

Route::namespace('Front')->group(function () {
    Route::get('/', 'FrontPageController@index')
        ->name('home');
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::namespace('Admin')->group(function () {
    Route::get('/dashboard', 'DashboardController@index')
        ->name('dashboard');

    /*
    |--------------------------------------------------------------------------
    | Company Routes
    |--------------------------------------------------------------------------
    */

    Route::prefix('companies')->group(function () {
        Route::get('/', 'CompanyController@index')
            ->name('companies-list');

        Route::get('/create', 'CompanyController@showCreateForm')
            ->name('create-company-form');

        Route::post('/', 'CompanyController@create')
            ->name('create-company');

        Route::get('/edit/{id}', 'CompanyController@showEditForm')
            ->name('edit-company-form');

        Route::patch('/{id}', 'CompanyController@update')
            ->name('update-company');

        Route::delete('/{id}', 'CompanyController@delete')
            ->name('delete-company');
    });
});
