<?php

namespace Tests\Feature\Auth;

use App\User;
use Tests\TestCase;
use Faker;

class AuthTest extends TestCase
{
    protected static $faker, $user, $email, $password;

    /**
     * Test case set up method.
     */
    protected function setUp()
    {
        parent::setUp();

        self::$faker = Faker\Factory::create();
        self::$email = self::$faker->email;
        self::$password = self::$faker->password;

        self::$user = factory(User::class)->make([
            'email'    => self::$email,
            'password' => bcrypt(self::$password),
        ]);
    }

    /**
     * Test case tear down method.
     */
    public function tearDown()
    {
        self::$user->forceDelete();

        parent::tearDown();
    }

    /**
     * Test login function.
     *
     * @test
     * @author janis@engineer.lv
     * @group  feature-test
     * @covers \Illuminate\Support\Facades\Auth::login();
     * @covers \Illuminate\Support\Facades\Auth::logout();
     */
    public function authTest()
    {
        // Go to front page
        $response = $this->get(route('home'));
        $response->assertSuccessful();
        $response->assertViewIs('home');

        // Go to login page
        $response = $this->get(route('login'));
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');

        // Go to dashboard and fail to login page
        $response = $this->get(route('dashboard'));
        $response->assertRedirect(route('login'));

        // Go to dashboard and fail to login page
        $response = $this->get(route('dashboard'));
        $response->assertRedirect(route('login'));

        // Check that auth user can't see login page
        $response = $this->actingAs(self::$user)->get(route('login'));
        $response->assertRedirect(route('dashboard'));

        // Auth user and get redirect to dashboard
        $response = $this->post(route('login'), [
            'email'    => self::$email,
            'password' => self::$password,
        ]);

        $response->assertRedirect(route('dashboard'));
        $this->assertAuthenticatedAs(self::$user);

        // Logout user and check if user is guest
        $response = $this->post(route('logout'));
        $response->assertRedirect(route('home'));
        $this->assertGuest();
    }
}
