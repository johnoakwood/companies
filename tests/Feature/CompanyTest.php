<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use Tests\TestCase;
use Faker;

class CompanyTest extends TestCase
{
    protected static $faker, $user;

    /**
     * Test case set up method.
     */
    protected function setUp()
    {
        parent::setUp();

        self::$faker = Faker\Factory::create();
        self::$user = factory(User::class)->make();

        $this->be(self::$user);
    }

    /**
     * Test case tear down method.
     */
    public function tearDown()
    {
        self::$user->forceDelete();

        parent::tearDown();
    }

    /**
     * Test companies list.
     *
     * @test
     * @author janis@engineer.lv
     * @group  feature-test
     * @covers \App\Http\Controllers\Admin\CompanyController::index()
     */
    public function companiesListTest()
    {
        $response = $this->json('GET', route('companies-list'));
        $response->assertStatus(200)
            ->assertViewHas('companies')
            ->assertViewIs('pages.admin.companies-list');
    }

    /**
     * Test create new company function.
     *
     * @test
     * @author janis@engineer.lv
     * @group  feature-test
     * @covers \App\Http\Controllers\Admin\CompanyController::showCreateForm()
     * @covers \App\Http\Controllers\Admin\CompanyController::create()
     */
    public function addCompanyTest()
    {
        // Test create company view
        $response = $this->json('GET', route('create-company-form'));
        $response->assertStatus(200);

        $companyData = [
            'name'     => self::$faker->name,
            'address'  => self::$faker->address,
            'homepage' => self::$faker->domainName,
            'email'    => self::$faker->companyEmail,
        ];

        $response = $this->post(route('create-company'), $companyData);
        $response->assertStatus(302)
            ->assertRedirect(route('companies-list'));

        $this->assertDatabaseHas('companies', $companyData);
    }

    /**
     * Test update company function.
     *
     * @test
     * @author janis@engineer.lv
     * @group  feature-test
     * @covers \App\Http\Controllers\Admin\CompanyController::showEditForm()
     * @covers \App\Http\Controllers\Admin\CompanyController::update()
     */
    public function editCompanyTest()
    {
        $companyData = [
            'name'     => self::$faker->name,
            'address'  => self::$faker->address,
            'homepage' => self::$faker->domainName,
            'email'    => self::$faker->companyEmail,
        ];

        $company = factory(Company::class)->make($companyData);
        $company->save();

        // Test edit company view
        $response = $this->json('GET', route('edit-company-form', $company->id));
        $response->assertStatus(200)
            ->assertViewIs('pages.admin.edit-company-form');

        // Set new data for company
        $companyData['name'] = self::$faker->name;
        $companyData['email'] = self::$faker->companyEmail;

        $response = $this->patch(route('update-company', $company->id), $companyData);
        $response->assertStatus(302)
            ->assertRedirect(route('companies-list'));

        $this->assertDatabaseHas('companies', $companyData);

        $company->delete();
    }

    /**
     * Test delete company function.
     *
     * @test
     * @author janis@engineer.lv
     * @group  feature-test
     * @covers \App\Http\Controllers\Admin\CompanyController::delete()
     */
    public function deleteCompanyTest()
    {
        $companyData = [
            'name'     => self::$faker->name,
            'address'  => self::$faker->address,
            'homepage' => self::$faker->domainName,
            'email'    => self::$faker->companyEmail,
        ];

        $company = factory(Company::class)->make($companyData);
        $company->save();

        $this->assertDatabaseHas('companies', $companyData);

        $response = $this->delete(route('delete-company', $company->id));
        $response->assertStatus(302)
            ->assertRedirect(route('companies-list'));

        $this->assertDatabaseMissing('companies', $companyData);
    }
}
