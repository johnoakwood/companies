<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class FrontPageController extends Controller
{
    /**
     * Show the application front page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
