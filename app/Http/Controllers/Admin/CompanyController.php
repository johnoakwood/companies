<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Company;
use App\Http\Requests\CompanyRequest;

class CompanyController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display list of companies.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $companies = Company::getCompanies(self::ITEMS_PER_PAGE);

        $viewData = [
            'companies' => $companies,
        ];

        return view('pages.admin.companies-list', $viewData);
    }

    /**
     * Display new company creation form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreateForm()
    {
        return view('pages.admin.create-company-form');
    }

    /**
     * Create and store new company.
     *
     * @param  \App\Http\Requests\CompanyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CompanyRequest $request)
    {
        $company = new Company([
            'name'     => $request->get('name'),
            'address'  => $request->get('address'),
            'homepage' => $request->get('homepage'),
            'email'    => $request->get('email'),
        ]);

        $company->save();

        return redirect(route('companies-list'))
            ->with('success', trans('messages.company_added'));
    }

    /**
     * Display company edit form.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showEditForm($id)
    {
        $company = Company::find($id);

        return view('pages.admin.edit-company-form', compact('company'));
    }

    /**
     * Update company data.
     *
     * @param  \App\Http\Requests\CompanyRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request->get('name');
        $company->address = $request->get('address');
        $company->homepage = $request->get('homepage');
        $company->email = $request->get('email');

        $company->save();

        return redirect(route('companies-list'))
            ->with('success', trans('messages.company_updated'));
    }

    /**
     * Delete company data.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $company = Company::find($id);
        $company->delete();

        return redirect(route('companies-list'))
            ->with('success', trans('messages.company_deleted'));
    }
}
