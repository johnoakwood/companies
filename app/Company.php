<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'homepage', 'email',
    ];

    const ITEMS_PER_PAGE = 30;

    /**
     * Returns all companies.
     *
     * @param  int $count
     * @return array
     */
    public static function getCompanies($count = self::ITEMS_PER_PAGE)
    {
        return Company::paginate($count);
    }
}
